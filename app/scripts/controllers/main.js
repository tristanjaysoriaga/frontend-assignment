'use strict';

/**
 * @ngdoc function
 * @name angularTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularTestApp
 */
angular.module('angularTestApp')
  .controller('MainCtrl', function ($scope, $http) {

  	$scope.init = function () {
  		//init
    	$scope.totalCount = 0;

  		//json call
		$scope.dataset = [];
	    $http.get('data/myData.json').success(function(response) {
	    	$scope.dataset = response;
	    	$scope.totalCount = response.length;
	  	});
  	};


	//load mobile desc
    $scope.mobile_showDescription = function(id) {

    	//reset bg color and icons
    	for(var i=0; i<=$scope.totalCount; i++){
    		$('#heading' + i).css('background-color', 'transparent');
    		$('#heading' + i).find('span').removeClass().addClass('glyphicon glyphicon-plus');

    		//hide all desc
    		$('#collapse' + i).hide();
    	}

    	//show description
    	$('#collapse' + id).show();

    	//set bg color
    	$('#heading' + id).css('background-color', '#fedb00');

    	//set the icon to plus
    	$('#heading' + id).find('span').removeClass().addClass('glyphicon glyphicon-minus');
    };


  	//load desktop desc
    $scope.desktop_showDescription = function(id) {
		
		//reset bg color
		for(var i=0; i<=$scope.totalCount; i++){
			$("#nav" + i).css('background-color', 'transparent');
		}

		//set bg color
		$("#nav" + id).css('background-color', '#fedb00');

		//set desc view
		for(var k=0; k<$scope.totalCount; k++){
		 	if($scope.dataset[k].id == id){
		 		$scope.title = $scope.dataset[k].title;	
		 		$scope.description = $scope.dataset[k].desc;
		 	}
		}
    };
  });
