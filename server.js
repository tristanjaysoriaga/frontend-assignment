var express = require("express");
var path = require("path");
var app     = express();

app.set('port', process.env.PORT || 8080);

app.use(express.static(path.join(__dirname, '/dist')));

app.get('/', function(req,res) {
  res.sendfile(path.join(__dirname, '/dist/index.html'));
});

// app.use(express.static(__dirname + '/dist'));

// app.engine('.html', require('ejs').__express);

// // Optional since express defaults to CWD/views

// //app.set('views', __dirname + '/views');

// // Without this you would need to
// // supply the extension to res.render()
// // ex: res.render('users.html').
// app.set('view engine', 'html');

// app.get('/',function(req,res){
//   res.render('/dist/index.html');
// });


app.listen(app.get('port'));
console.log("Running");